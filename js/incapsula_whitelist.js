var app = angular.module("incapsulaApp", []);
app.controller("ctrlwhitelist", function ($scope, $http) {
    $scope.rows = [];
    $scope.temp = false;
    $scope.saving = false;
    $scope.invalidip = false;
    $scope.addRegion == "";

    $http.get('/incapsula/endpoint/config')
        .then(function (res) {
            var acls = res.data.security.acls.rules;
            for (var i = 0; i < acls.length; i++) {
                if (acls[i].id == 'api.acl.whitelisted_ips') {
                    $scope.rows = acls[i].ips;
                    break;
                }
            }
            $scope.dataLoaded = true;
        });

    $scope.addRow = function () {
        if (validip($scope.addIp)) {
            $scope.temp = false;
            $scope.addIp = "";
        }
        else {

        }
    };

    $scope.deleteRow = function (row) {
        $scope.rows.splice($scope.rows.indexOf(row), 1);
    };

    $scope.plural = function (tab) {
        return (tab.length > 1 || tab.length == 0) ? 's' : '';
    };

    $scope.addTemp = function () {
        if ($scope.addIp != "" && !validip($scope.addIp)) $scope.invalidip = true;
        else if ($scope.addIp == "" || validip($scope.addIp)) $scope.invalidip = false;

        if ($scope.temp) $scope.rows.pop();
        else if ($scope.addIp) $scope.temp = true;

        if ($scope.addIp) $scope.rows.push($scope.addIp);
        else $scope.temp = false;
    };

    $scope.isTemp = function (index) {
        return $scope.rows[index] == $scope.addIp;
    };

    $scope.saveForm = function (rows) {
        if ($scope.temp == false && $scope.addIp == "") {
            $scope.saving = true;

            $http({
                method: 'POST',
                url: '/incapsula/endpoint/whitelist',
                data: "ips=" + rows.join(","),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function (res) {
                $scope.saving = false;
            });
        }
    };

    function validip(str) {
        return /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$|^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$/.test(str);
    }
});
