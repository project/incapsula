<?php
/**
 * @file
 * Configuration pages for the incapsula module.
 */

/**
 * Configure the Incapsula account.
 * @ingroup forms
 */
function incapsula_configuration_form($form, &$form_state) {
  $form['incapsula'] = [
    '#type' => 'fieldset',
    '#title' => t('Incapsula integration'),
    '#description' => t('Adjust the settings for your Incapsula API connection.'),
  ];

  $form['incapsula']['incapsula_api_id'] = [
    '#type' => 'textfield',
    '#title' => t('API ID'),
    '#size' => 25,
    '#default_value' => variable_get('incapsula_api_id', ''),
  ];

  $form['incapsula']['incapsula_api_key'] = [
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#size' => 25,
    '#default_value' => variable_get('incapsula_api_key', ''),
  ];

  $form['incapsula']['incapsula_site_id'] = [
    '#type' => 'textfield',
    '#title' => t('Site ID'),
    '#size' => 25,
    '#default_value' => variable_get('incapsula_site_id', ''),
  ];

  return system_settings_form($form);
}

/**
 * Configure the Incapsula whitelisted ip's.
 * @ingroup forms
 */
function incapsula_whitelist_form() {
  return theme('incapsula_whitelist', []);
}

/**
 * Configure the Incapsula blocked ip's.
 * @ingroup forms
 */
function incapsula_block_ips_form($form, &$form_state) {
  $form['incapsula'] = [
    '#markup' => theme('incapsula_block_ips', []),
  ];

  return $form;
}

/**
 * Configure the Incapsula blocked URLs.
 * @ingroup forms
 */
function incapsula_block_url_form($form, &$form_state) {
  $form['incapsula'] = [
    '#markup' => theme('incapsula_block_urls', []),
  ];

  return $form;
}

/**
 * Configure the Incapsula block countries.
 * @ingroup forms
 */
function incapsula_block_country_form($form, &$form_state) {
  $form['incapsula'] = [
    '#markup' => theme('incapsula_block_countries', []),
  ];

  return $form;
}
