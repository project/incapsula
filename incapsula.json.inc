<?php
/**
 * @file
 * JSON callbacks for the incapsula module.
 */


/**
 * Endpoint callback for the management pages.
 * @param $endpoint
 */
function incapsula_endpoint_drupal($endpoint) {
  $function = '_incapsula_endpoint_drupal_' . $endpoint;
  if (function_exists($function)) {
    echo $function();
    exit(0);
  }
  echo drupal_json_output(['error' => 'JSON endpoint doesn\'t exist']);
  exit(0);
}

/**
 * Helper function to fetch the configuration from Incapsula.
 */
function _incapsula_endpoint_drupal_config() {
  if ($inc = _incapsula_object_construct()) {
    echo drupal_json_output($inc->config());
  }
  else {
    echo drupal_json_output(['error' => 'Incapsula isn\'t configured, go the configuration page to resolve this']);
  }
}

/**
 * Helper function to manage the IP whitelist.
 */
function _incapsula_endpoint_drupal_whitelist() {
  $ips = isset($_POST['ips']) ? $_POST['ips'] : "";
  if ($inc = _incapsula_object_construct()) {
    echo drupal_json_output($inc->acl(INCAPSULA_ACL_WHITELIST_IP, $ips));
  }
  else {
    echo drupal_json_output(['error' => 'Incapsula isn\'t configured, go the configuration page to resolve this']);
  }
}

/**
 * Helper function to manage the IP blacklist.
 */
function _incapsula_endpoint_drupal_blockips() {
  $ips = isset($_POST['ips']) ? $_POST['ips'] : "";
  if ($inc = _incapsula_object_construct()) {
    echo drupal_json_output($inc->acl(INCAPSULA_ACL_BLACKLIST_IP, $ips));
  }
  else {
    echo drupal_json_output(['error' => 'Incapsula isn\'t configured, go the configuration page to resolve this']);
  }
}

/**
 * Helper function to manage the blocked URLs.
 */
function _incapsula_endpoint_drupal_blockurls() {
  $urls = isset($_POST['urls']) ? $_POST['urls'] : "";
  $pattern = isset($_POST['pattern']) ? $_POST['pattern'] : "";
  if ($inc = _incapsula_object_construct()) {
    echo drupal_json_output($inc->acl(INCAPSULA_ACL_BLACKLIST_URLS, [
      'urls' => str_replace("%2C", ",", urlencode($urls)),
      'url_patterns' => $pattern
    ]));
  }
  else {
    echo drupal_json_output(['error' => 'Incapsula isn\'t configured, go the configuration page to resolve this']);
  }
}

/**
 * Helper function to manage the blocked countries/contintents.
 */
function _incapsula_endpoint_drupal_blockcountry() {
  $countries = isset($_POST['countries']) ? $_POST['countries'] : "";
  $continents = isset($_POST['continents']) ? $_POST['continents'] : "";
  if ($inc = _incapsula_object_construct()) {
    echo drupal_json_output($inc->acl(INCAPSULA_ACL_BLACKLIST_COUNTRY, [
      'countries' => $countries,
      'continents' => $continents
    ]));
  }
  else {
    echo drupal_json_output(['error' => 'Incapsula isn\'t configured, go the configuration page to resolve this']);
  }
}
