var app = angular.module("incapsulaApp", []);
app.controller("ctrlblockcountries", function ($scope, $http) {
    $scope.rows = [];
    $scope.temp = false;
    $scope.saving = false;
    $scope.addRegion = "";

    $http.get('/incapsula/endpoint/config')
        .then(function (res) {
            var acls = res.data.security.acls.rules;
            for (var i = 0; i < acls.length; i++) {
                if (acls[i].id == 'api.acl.blacklisted_countries') {
                    for (var prop in acls[i].geo) {
                        if (prop == 'countries') {
                            var countries = acls[i].geo.countries;
                            for (var i = 0; i < countries.length; i++) {
                                $scope.rows.push({
                                    region: countries[i],
                                    type: 'country'
                                });
                            }
                        }
                        if (prop == 'continents') {
                            var continents = acls[i].geo.continents;
                            for (var i = 0; i < continents.length; i++) {
                                $scope.rows.push({
                                    region: continents[i],
                                    type: 'continent'
                                });
                            }
                        }
                    }
                    break;
                }
            }
            $scope.dataLoaded = true;
        });

    $scope.addRow = function () {
        $scope.temp = false;
        $scope.addRegion = "";
        $scope.regionType = "country";
    };

    $scope.deleteRow = function (row) {
        $scope.rows.splice($scope.rows.indexOf(row), 1);
    };

    $scope.plural = function (tab) {
        return (tab.length > 1 || tab.length == 0) ? 's' : '';
    };

    $scope.addTemp = function () {
        if ($scope.temp) $scope.rows.pop();
        else if ($scope.addRegion) $scope.temp = true;
        if ($scope.addRegion) {
            $scope.rows.push({
                region: $scope.addRegion,
                type: $scope.regionType
            });
        }
        else {
            $scope.temp = false;
        }
    };

    $scope.isTemp = function (index) {
        return $scope.rows[index].region == $scope.addRegion;
    };

    $scope.saveForm = function (rows) {
        if ($scope.temp == false && $scope.addRegion == "") {
            $scope.saving = true;

            var countries = [];
            var continents = [];
            jQuery.each(rows, function (key, row) {
                if (row.type == 'country' && countries.indexOf(row.region) < 0) {
                    countries.push(row.region);
                }
                else if (row.type == 'continent' && continents.indexOf(row.region) < 0) {
                    continents.push(row.region);
                }
            });

            $http({
                method: 'POST',
                url: '/incapsula/endpoint/blockcountry',
                data: "continents=" + continents.join(",") + "&countries=" + countries.join(","),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function (res) {
                $scope.saving = false;
            });
        }
    };
});
