<?php
/**
 *
 */

?>
<div ng-controller="ctrlblockcountries">
    <h2>{{rows.length}} Blocked region{{plural(rows)}} <span ng-show="temp">?<small
                    class="muted"><em> (only {{rows.length-1}} actually....)</em></small></span>
    </h2>
    <div class="text-center" ng-hide="dataLoaded">
        <i class="icon-loading-animate icon-refresh icon-black"></i> Loading
        data
    </div>
    <div ng-show="dataLoaded">
        <form class="form-horizontal">
            <fieldset class="control-group form-inline">
            <span ng-class="{'input-append':addRegion}">
                <select id="pattern" ng-show="addRegion" ng-change="addTemp()" ng-model="regionType">
                    <option value="country">Country</option>
                    <option value="continent">Continent</option>
                </select>
                <input id="add" type="text" placeholder="Add another region?"
                       ng-model="addRegion" ng-change="addTemp()"/>
                <input type="button" class="btn btn-primary" ng-click="addRow()"
                       ng-show="addRegion" value="+ add"/>

            </span>
            </fieldset>
            <fieldset class="control-group form-inline">
        <span class="search input-prepend" ng-class="{'input-append':search}">
            <span class="add-on"><i class="icon-search"></i></span>
            <input type="text" class="span2" placeholder="Search"
                   ng-model="search">
            <button type="button" class="btn btn-inverse" ng-click="search=''"
                    ng-show="search" value="+ add">
                <i class="icon-remove icon-white"></i>
            </button>
        </span>
            </fieldset>
        </form>
        <table class="table table-striped">
            <tr>
                <th>#</th>
                <th>Region</th>
                <th>Type</th>
                <th>actions</th>
            </tr>
            <tr ng-repeat="row in rows | filter : search"
                ng-class="{'muted':isTemp($index)}">
                <td>{{$index+1}}</td>
                <td>{{row.region}}</td>
                <td>{{row.type}}</td>
                <td>
                    <button class="btn btn-danger btn-mini"
                            ng-click="deleteRow(row)" ng-hide="isTemp($index)">
                        <i
                                class="icon-trash icon-white"></i></button>
                </td>
            </tr>
        </table>
        <button type="button" class="btn btn-success"
                ng-click="saveForm(rows)" ng-show="dataLoaded"><i
                    class="icon-ok icon-white"></i> Save
        </button>
        <span class="text-center" ng-show="saving"><i
                    class="icon-loading-animate icon-refresh icon-black"></i> Saving</span>
    </div>
</div>
