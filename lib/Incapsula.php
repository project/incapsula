<?php

define('INCAPSULA_ACL_BLACKLIST_COUNTRY', 'api.acl.blacklisted_countries');
define('INCAPSULA_ACL_BLACKLIST_URLS', 'api.acl.blacklisted_urls');
define('INCAPSULA_ACL_BLACKLIST_IP', 'api.acl.blacklisted_ips');
define('INCAPSULA_ACL_WHITELIST_IP', 'api.acl.whitelisted_ips');

class Incapsula {
  /**
   * Incapsula API endpoint
   */
  var $api_endpoint = "https://my.incapsula.com/api/prov/v1";
  var $api_id;
  var $api_key;
  var $site_id;

  /**
   *
   */
  function __construct($api_id = NULL, $api_key = NULL, $site_id = NULL) {
    $this->api_id = $api_id;
    $this->api_key = $api_key;
    $this->site_id = $site_id;
  }

  /**
   * Purge callback to clear cache for a page or resource.
   *
   * @param string $pattern
   *   The pattern or exact string to purge from incapsula cache.
   *
   * @param string $site_id
   *   The site id as defined in incapsula allowing an override.
   *
   * @return mixed
   *   Returns a response-array from incapsula.
   */
  function purge($pattern = NULL, $site_id = NULL) {
    $params = $this->settings($site_id);
    $params['purge_pattern'] = !is_null($pattern) ? $pattern : '';

    return $this->api_call('/sites/cache/purge', $params);
  }

  function config($site_id = NULL) {
    $params = $this->settings($site_id);
    return $this->api_call('/sites/status', $params);

  }

  function acl($rule = NULL, $options = "", $deviate = FALSE, $fetch = TRUE, $site_id = NULL) {
    $default_acl = [
      'rule_id' => $rule,
      'continents' => '',
      'countries' => '',
      'url_patterns' => '',
      'urls' => '',
      'ips' => '',
    ];
    $params = array_merge($this->settings($site_id), $default_acl);
    switch ($rule) {
      case INCAPSULA_ACL_BLACKLIST_COUNTRY:
        if (!(is_array($options))) {
          watchdog('Incapsula', 'API call for @rule requires an array.', ['@rule' => $rule], WATCHDOG_ERROR);
          return;
        }
        if (isset($options['countries'])) {
          $params['countries'] = $options['countries'];
        }
        if (isset($options['continents'])) {
          $params['continents'] = $options['continents'];
        }
        break;

      case INCAPSULA_ACL_BLACKLIST_URLS:
        if (!(is_array($options))) {
          watchdog('Incapsula', 'API call for @rule requires an array.', ['@rule' => $rule], WATCHDOG_ERROR);
          return;
        }
        if (!isset($options['urls']) || !isset($options['url_patterns'])) {
          watchdog('Incapsula', 'API call for @rule requires an array with keys \'urls\' and \'url_patterns\'.', ['@rule' => $rule], WATCHDOG_ERROR);
          return;
        }
        $params['urls'] = $options['urls'];
        $params['url_patterns'] = $options['url_patterns'];
        break;

      case INCAPSULA_ACL_BLACKLIST_IP:
      case INCAPSULA_ACL_WHITELIST_IP:
        $params['ips'] = $options;
        break;
    }

    return $this->api_call('/sites/configure/acl', $params);
  }

  private function api_call($api_url, $params = [], $method = 'POST') {
    $endpoint = $this->api_endpoint . $api_url;
    $response = NULL;
    switch ($method) {
      case 'POST':
        $options = [
          'method' => $method,
          'data' => http_build_query($params),
          'timeout' => 15,
          'headers' => [
            'Content-Type' => 'application/x-www-form-urlencoded',
            'X-Requested-With' => 'XMLHttpRequest'
          ],
        ];
        $response = drupal_http_request($endpoint, $options);
        break;
    }

    if ($response->code == 200) {
      // Valid response, get pages and objects.
      $data = json_decode($response->data);
      return $data;
    }
    else {
      watchdog('Incapsula', 'API call failed with status code @code', ['@code' => $response->code], WATCHDOG_ERROR);
    }

  }

  private function settings($site_id = NULL) {
    return [
      'api_id' => $this->api_id,
      'api_key' => $this->api_key,
      'site_id' => is_null($site_id) ? $this->site_id : $site_id,
    ];
  }
}
