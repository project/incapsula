<?php
/**
 * @file
 */

/**
 * Implements hook_variable_group_info().
 */
function incapsula_variable_group_info() {
  $groups = [];

  $groups['incapsula_vars'] = [
    'title' => t('Incapsula'),
    'access' => 'administer site configuration',
  ];

  return $groups;
}

/**
 * Implements hook_variable_info().
 */
function incapsula_variable_info() {
  $variables = [];

  $variables['incapsula_api_id'] = [
    'type' => 'string',
    'title' => t('API ID'),
    'group' => 'incapsula_vars',
  ];

  $variables['incapsula_api_key'] = [
    'type' => 'string',
    'title' => t('API Key'),
    'group' => 'incapsula_vars',
  ];

  $variables['incapsula_site_id'] = [
    'type' => 'string',
    'title' => t('Site ID'),
    'group' => 'incapsula_vars',
  ];

  return $variables;
}