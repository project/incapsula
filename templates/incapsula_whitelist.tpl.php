<?php
/**
 *
 */

?>
<div ng-controller="ctrlwhitelist">
    <h2>{{rows.length}} Whitelisted IP{{plural(rows)}} <span ng-show="temp">?<small
                    class="muted"><em> (only {{rows.length-1}} actually....)</em></small></span>
    </h2>
    <div class="text-center" ng-hide="dataLoaded">
        <i class="icon-loading-animate icon-refresh icon-black"></i> Loading data
    </div>
    <div ng-show="dataLoaded">
    <form class="form-horizontal">
        <fieldset class="control-group form-inline" ng-class="{'error':invalidip}">
            <span ng-class="{'input-append':addIp}">
                <input id="add" type="text" placeholder="Add another IP?" ng-model="addIp" ng-change="addTemp()"/>
                <input type="button" class="btn btn-primary" ng-click="addRow()" ng-show="addIp" value="+ add"/>
            </span>
            <span class="help-inline" ng-show="invalidip">Please correct the IP</span>
        </fieldset>
        <fieldset class="control-group form-inline">
        <span class="search input-prepend" ng-class="{'input-append':search}">
            <span class="add-on"><i class="icon-search"></i></span>
            <input type="text" class="span2" placeholder="Search" ng-model="search">
            <button type="button" class="btn btn-inverse" ng-click="search=''" ng-show="search" value="+ add">
                <i class="icon-remove icon-white"></i>
            </button>
        </span>
        </fieldset>
    </form>
    <table class="table table-striped">
        <tr ng-repeat="row in rows | filter : search"
            ng-class="{'muted':isTemp($index)}">
            <td>{{$index+1}}</td>
            <td>{{row}}</td>
            <td>
                <button class="btn btn-danger btn-mini"
                        ng-click="deleteRow(row)" ng-hide="isTemp($index)"><i
                            class="icon-trash icon-white"></i></button>
            </td>
        </tr>
    </table>
    <button type="button" class="btn btn-success"
            ng-click="saveForm(rows)" ng-show="dataLoaded"><i
                class="icon-ok icon-white"></i> Save</button><span class="text-center" ng-show="saving"><i class="icon-loading-animate icon-refresh icon-black"></i> Saving</span>
    </div>
</div>
